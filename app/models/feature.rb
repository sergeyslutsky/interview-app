class Feature < ApplicationRecord
  belongs_to :user
  has_one :feature_variant, dependent: :destroy

  def feature_info
    { name => feature_variant.value }
  end
end
