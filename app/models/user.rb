class User < ApplicationRecord
  has_many :features, dependent: :destroy

  def self.find_or_create(token)
    find_by(token: token) || create_user(token)
  end

  def experiment_list
    features.includes(:feature_variant).map(&:feature_info)
  end

  class << self

    private

    # Notice: ExperimentGenerator returns array of hashes.
    # Each hash corresponds to the experiment and contains of two elements:
    # { name: feature_name, value: feature_variant }
    def create_user(token)
      user = create(token: token)
      experiments = ExperimentGenerator.call
      experiments.each do |exp|
        feature = user.features.create(name: exp[:name])
        feature.create_feature_variant(value: exp[:value])
      end
      user
    end
  end
end
