# Notice: do not initialize any constants in ExperimentGenerator namespace
# except an experiment as a nested module
module ExperimentGenerator
  class << self
    def call
      experiments.map do |exp|
        exp_object         = {}
        exp_object[:name]  = exp::NAME
        exp_object[:value] = value_generator(exp::VALUES_WITH_WEIGHTS)
        exp_object
      end
    end

    def experiments
      ExperimentGenerator.constants.map(&method(:const_get))
    end

    private

    def value_generator(values)
      max = sum_of_weights(values)
      raise ExceptionHandler::IncorrectWeights unless max.is_a?(Integer) && max.positive?

      target = Random.rand(1..max)
      values.each do |value, weight|
        return value if target <= weight

        target -= weight
      end
    end

    def sum_of_weights(values)
      values.inject(0) { |sum, (_value, weight)| sum + weight }
    end
  end
end
