require_relative 'experiment_generator'

# Notice: only natural numbers can be as weights
module ExperimentGenerator::PriceExperiment
  NAME = :price
  VALUES_WITH_WEIGHTS = {
    '10' => 15,
    '20' => 2,
    '50' => 1,
    '5'  => 2
  }.freeze
end
