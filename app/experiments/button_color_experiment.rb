require_relative 'experiment_generator'

# Notice: only natural numbers can be as weights
module ExperimentGenerator::ButtonColorExperiment
  NAME = :button_color
  VALUES_WITH_WEIGHTS = {
    '#FF0000' => 1,
    '#00FF00' => 1,
    '#0000FF' => 1
  }.freeze
end
