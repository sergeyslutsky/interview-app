module StatisticsHelper
  def generate_table_data
    main_table = []
    ExperimentGenerator.experiments.each do |exp|
      exp_name = exp::NAME
      standalone_table = {
        experiment: exp_name,
        total_devices: Feature.where(name: exp_name).count,
        variants_with_devices_count: collect_data(exp)
      }
      main_table << standalone_table
    end
    main_table
  end

  private

  def collect_data(exp)
    query = FeatureVariant.includes(:feature).joins(:feature).where(features: { name: exp::NAME })
    available_variants = query.distinct.pluck(:value)
    result = []
    available_variants.each do |variant|
      device_count = query.where(feature_variants: { value: variant }).count
      result << { variant: variant, device_count: device_count }
    end
    result
  end
end
