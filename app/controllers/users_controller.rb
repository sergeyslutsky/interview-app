class UsersController < ApplicationController
  before_action :find_by_token_or_create, only: :show

  def show
    render_response @user.experiment_list
  end

  private

  def find_by_token_or_create
    @user = User.find_or_create params[:token]
  end
end
