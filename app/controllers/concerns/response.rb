module Response
  def render_response(message, status = 200)
    render json: message, status: status
  end
end
