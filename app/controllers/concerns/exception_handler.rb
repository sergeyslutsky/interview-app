module ExceptionHandler
  extend ActiveSupport::Concern

  # Custom error subclasses - rescue catches `StandardErrors`
  class IncorrectWeights < StandardError; end

  included do
    # Custom handlers
    rescue_from ExceptionHandler::IncorrectWeights, with: :weights_error
  end

  private

  def weights_error
    render_response('There is an experiment with incorrect weights', 422)
  end

end
