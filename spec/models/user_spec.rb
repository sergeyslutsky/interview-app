require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'find_or_create functional' do
    def stub_call_method(return_value)
      allow(ExperimentGenerator).to receive(:call).and_return(return_value)
    end

    context 'user does not exist' do
      def fire_find_or_create_method
        User.find_or_create("token#{rand(20)}")
      end

      context 'experiments do not exist' do
        return_value = []
        before do
          stub_call_method(return_value)
        end
        let(:user) { fire_find_or_create_method }

        it 'creates new user, do not creates feature and feature_variant' do
          expect(user.id).to be_an Integer
          expect(user.features).to eq []
        end
      end

      context '1 experiment exists' do
        return_value = [{ name: 'price', value: '10' }]
        before do
          stub_call_method(return_value)
        end
        let(:user) { fire_find_or_create_method }

        it 'creates new user, feature and feature_variant' do
          expect(user.id).to be_an Integer
          expect(user.features.count).to eq 1
          expect(user.features.first.feature_variant.value).to eq '10'
        end
      end
    end

    context 'user exists' do
      token = "some_token#{rand(999)}"
      before do
        stub_call_method([{ name: 'some_experiment', value: 'asd' }])
      end
      let(:user) { FactoryBot.create(:user, token: token) }
      let!(:user_feature) { user.features }

      it 'just finds user, not create' do
        found_user = User.find_or_create(token)
        expect(found_user.id).to eq(user.id)
        expect(user.features).to eq []
      end
    end
  end

  describe 'experiment_list functional' do
    let(:user) { FactoryBot.create :user }

    context 'there is no features' do
      it 'returns empty array' do
        expect(user.experiment_list).to eq []
      end
    end

    context 'there is feature with feature_variant' do
      let(:feature) { FactoryBot.create(:feature, user_id: user.id) }
      let!(:feature_variant) { FactoryBot.create(:feature_variant, feature_id: feature.id) }

      it 'returns array of one hash' do
        expect(user.experiment_list).to eq([{ 'some_feature' => 'some_value' }])
      end
    end
  end
end
