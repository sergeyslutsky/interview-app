require 'rails_helper'

RSpec.describe ExperimentGenerator do
  describe 'value_generator functional' do
    context 'incorrect experiment weights' do
      let(:zero_weights) { { a: 0, b: 0 } }
      let(:negative_weights) { { a: -1, b: -2 } }
      let(:float_weights) { { a: 10.0, b: 2.5 } }

      it 'raises the error' do
        expect { ExperimentGenerator.send(:value_generator, zero_weights) }
          .to raise_error(ExceptionHandler::IncorrectWeights)
        expect { ExperimentGenerator.send(:value_generator, negative_weights) }
          .to raise_error(ExceptionHandler::IncorrectWeights)
        expect { ExperimentGenerator.send(:value_generator, float_weights) }
          .to raise_error(ExceptionHandler::IncorrectWeights)
      end
    end

    context 'correct experiment weights' do
      let(:right_weights) { { a: 10, b: 2 } }

      it 'returns a or b' do
        expect(ExperimentGenerator.send(:value_generator, right_weights)).to eq(:a).or eq(:b)
      end
    end
  end

  describe 'call functional' do
    def stub_experiments_method(return_value)
      allow(ExperimentGenerator).to receive(:experiments).and_return(return_value)
    end

    context 'there are no experiments' do
      before do
        stub_experiments_method []
      end

      it 'returns empty array' do
        expect(ExperimentGenerator.call).to eq []
      end
    end

    context 'there are two experiments' do
      let(:generator_test) do
        module GeneratorTest; end
        module GeneratorTest::Test1
          NAME = 'test1'.freeze
          VALUES_WITH_WEIGHTS = { 'opt1_1' => 1, 'opt1_2' => 2 }.freeze
        end
        module GeneratorTest::Test2
          NAME = 'test2'.freeze
          VALUES_WITH_WEIGHTS = { 'opt2_1' => 2, 'opt2_2' => 2 }.freeze
        end
        GeneratorTest
      end

      before do
        return_value = generator_test.constants.map { |submodule| GeneratorTest.const_get(submodule) }
        stub_experiments_method return_value
      end

      it 'returns array of hashes, each one contains experiment data' do
        expect(ExperimentGenerator.call.first[:name]).to eq 'test1'
        expect(ExperimentGenerator.call.first[:value]).to eq('opt1_1').or eq('opt1_2')
        expect(ExperimentGenerator.call.last[:name]).to eq 'test2'
        expect(ExperimentGenerator.call.last[:value]).to eq('opt2_1').or eq('opt2_2')
      end
    end
  end
end
