class AddIndexToFeaturesOnTwoColumns < ActiveRecord::Migration[5.2]
  def change
    remove_index :features, :user_id
    add_index :features, %i[name user_id], unique: true
  end
end
