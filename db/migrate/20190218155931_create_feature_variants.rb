class CreateFeatureVariants < ActiveRecord::Migration[5.2]
  def change
    create_table :feature_variants do |t|
      t.string :value
      t.references :feature, foreign_key: true, index: true
      t.timestamps
    end
  end
end
