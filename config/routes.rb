Rails.application.routes.draw do
  root 'statistics#index'
  scope :api do
    resources :users, only: :show, param: :token
  end
  resources :statistics, only: :index
  get '*path' => redirect('/')
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
